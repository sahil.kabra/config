function avroToJson --argument path
  if test -d $path
    for f in $path/*.avro
      java -jar /opt/avro-tools/avro-tools-1.11.2.jar tojson $f > $f.json
    end
  else
      java -jar /opt/avro-tools/avro-tools-1.11.2.jar tojson $path > $path.json
  end
  echo "done" $status
end
