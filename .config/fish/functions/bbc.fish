function bbc --argument workspace repo
    mkdir -p ~/src/work/$repo
    cd ~/src/work/$repo
    git clone --bare git@bitbucket.org:$workspace/$repo.git .bare
    ln -s .bare .git
    echo "tags" >> .git/info/exclude
    echo ".tool-versions" >> .git/info/exclude
    git config remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*'
    git f
end
