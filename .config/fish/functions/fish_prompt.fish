function fish_prompt --description 'Write out the prompt'
  set last_status $status

  set_color $fish_color_cwd
  printf '%s' (prompt_pwd)

  set_color normal

  printf '%s' (fish_git_prompt)

  printf ' %s ' (aws_prompt)

  printf '\n> '

  set_color normal
end

function aws_prompt --description "helper function to show aws status"
  if test -z $AWS_VAULT
    return 1
  else
    printf "(%s)" (echo $AWS_VAULT)
  end
end
