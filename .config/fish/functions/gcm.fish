# Defined in /home/sahil-kabra_skyfii/.aliases @ line 54
function gcm --argument repo
    mkdir -p ~/src/personal/$repo
    cd ~/src/personal/$repo
    git clone --bare git@gitlab.com:sahil.kabra/$repo.git .bare
    ln -s .bare .git
    echo "tags" >> .git/info/exclude
    echo ".tool-versions" >> .git/info/exclude
    git config remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*'
    git f
end
