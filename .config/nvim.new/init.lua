local load = function(mod)
	package.loaded[mod] = nil
	require(mod)
end

load('user.before')
load('user.lazy')
load('user.settings')
load('user.mappings')
