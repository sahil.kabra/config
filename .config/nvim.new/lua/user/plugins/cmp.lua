local Plugin = {'hrsh7th/nvim-cmp'}

Plugin.dependencies = {
  -- Sources
  {'hrsh7th/cmp-buffer'},
  {'hrsh7th/cmp-path'},
  { 'f3fora/cmp-spell' },
  { 'quangnguyen30192/cmp-nvim-tags' },
  { 'ray-x/cmp-treesitter' },
  { 'andersevenrud/cmp-tmux' },
	{ "zbirenbaum/copilot-cmp" },

  -- Snippets
  { 'hrsh7th/cmp-vsnip' },
  { 'hrsh7th/vim-vsnip' },
}

Plugin.event = 'InsertEnter'

function Plugin.config()
  vim.opt.completeopt = {'menu', 'menuone', 'noselect'}

  local cmp = require('cmp')

  local select_opts = {behavior = cmp.SelectBehavior.Select}

  -- See :help cmp-config
  cmp.setup({
    snippet = {
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body)
      end
    },
    sources = {
      { name = 'copilot'},
      { name = 'nvim_lsp'},
      { name = 'buffer' },
      { name = 'tmux'},
      { name = 'path' },
      { name = 'spell' },
    },
    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },
    formatting = {
      fields = {'menu', 'abbr', 'kind'},
      format = function(entry, item)
        local menu_icon = {
          nvim_lsp = 'λ',
          luasnip = '⋗',
          buffer = 'Ω',
          path = '🖫',
          copilot = '🤖'
        }

        item.menu = menu_icon[entry.source.name]
        return item
      end,
    },
    -- See :help cmp-mapping
    mapping = {
      ['<Up>'] = cmp.mapping.select_prev_item(select_opts),
      ['<Down>'] = cmp.mapping.select_next_item(select_opts),

      ['<C-p>'] = cmp.mapping.select_prev_item(select_opts),
      ['<C-n>'] = cmp.mapping.select_next_item(select_opts),

      ['<C-j>'] = cmp.mapping.scroll_docs(-4),
      ['<C-k>'] = cmp.mapping.scroll_docs(4),

      ['<C-e>'] = cmp.mapping.abort(),
      ['<C-f>'] = cmp.mapping.confirm({select = true}),
      ['<CR>'] = cmp.mapping.confirm({select = false}),
    },
  })
end

return Plugin
