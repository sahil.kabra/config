local Plugin = { "zbirenbaum/copilot.lua" }

Plugin.lazy = false

Plugin.dependencies = {
	{ "zbirenbaum/copilot-cmp" },
}

Plugin.opts = {
	panel = {
		enabled = false,
		auto_refresh = false,
		keymap = {
			jump_prev = "[[",
			jump_next = "]]",
			accept = "<CR>",
			refresh = "gr",
			open = "<M-CR>",
		},
		layout = {
			position = "bottom", -- | top | left | right
			ratio = 0.4,
		},
	},
	suggestion = {
		enabled = false,
		auto_trigger = false,
		debounce = 75,
		keymap = {
			accept = "<M-l>",
			accept_word = false,
			accept_line = false,
			next = "<M-]>",
			prev = "<M-[>",
			dismiss = "<C-]>",
		},
	},
	filetypes = {
		javascript = true,
		typescript = true,
		lua = true,
		bash = true,
		yaml = false,
		markdown = false,
		help = false,
		gitcommit = false,
		gitrebase = false,
		hgcommit = false,
		svn = false,
		cvs = false,
		["."] = false,
	},
	copilot_node_command = "node", -- Node.js version must be > 18.x
	server_opts_overrides = {},
}

Plugin.config = function(_, opts)
  require("copilot").setup(opts)
	require("copilot_cmp").setup()
end

return Plugin
