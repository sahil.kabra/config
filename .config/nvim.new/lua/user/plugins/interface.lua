return {
	'tmhedberg/SimpylFold',
	'junegunn/vim-easy-align',
	'editorconfig/editorconfig-vim',
	'tpope/vim-surround',
	'tpope/vim-sleuth',
	'tpope/vim-fugitive',
}
