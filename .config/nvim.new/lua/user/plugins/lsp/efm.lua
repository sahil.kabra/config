local mappings = require("user.plugins.lsp.mappings")
local lspconfig = require("lspconfig")
local capabilities = require("cmp_nvim_lsp").default_capabilities()

local eslint = require("efmls-configs.linters.eslint")
local prettier = require("efmls-configs.formatters.prettier")
local py_lint = require("efmls-configs.linters.ruff")
local py_format = require("efmls-configs.formatters.ruff")
local scalafmt = require("efmls-configs.formatters.scalafmt")
local jq_format = require("efmls-configs.formatters.jq")
local jq_lint = require("efmls-configs.linters.jq")

local languages = {
	typescript = { eslint, prettier },
	javascript = { eslint, prettier },
	html = { prettier },
	json = { jq_format, jq_lint },
	lua = {
		require("efmls-configs.formatters.stylua"),
	},
	python = { py_lint, py_format },
	scala = { scalafmt },
}

local efmls_config = {
	filetypes = vim.tbl_keys(languages),
	settings = {
		rootMarkers = { ".git/", ".gitignore" },
		languages = languages,
	},
	init_options = {
		documentFormatting = true,
		documentRangeFormatting = true,
	},
}

lspconfig.efm.setup(vim.tbl_extend("force", efmls_config, {
	on_attach = mappings.on_attach,
	capabilities = capabilities,
}))
