local lsp_installer = require("nvim-lsp-installer")
local mappings = require 'lsp/mappings'
local server_disable_formatting = {"html", "jsonls"}
local capabilities = require('cmp_nvim_lsp').default_capabilities()

local function has_value(arr, value)
  for _, v in pairs(arr) do
    if v == value then
      return true
    end
  end

  return false
end

lsp_installer.on_server_ready(function(server)
  local opts = {
    on_attach = function(client, bufnr)
      if has_value(server_disable_formatting, server) then
        client.server_capabilities.document_formatting = false
      end
      mappings.on_attach(client, bufnr)
    end,
    capabilities = capabilities
  }

  server:setup(opts)
end)
