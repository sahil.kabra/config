local mappings = require 'user.plugins.lsp.mappings'
local lspconfig = require("lspconfig")
local lsp_capabilities = require('cmp_nvim_lsp').default_capabilities()

local function organize_imports()
  local p = {
    command = "_typescript.organizeImports",
    arguments = {vim.api.nvim_buf_get_name(0)},
    title = ""
  }

  vim.lsp.buf.execute_command(p)
end


local on_attach = function(client, bufnr)
  client.server_capabilities.document_formatting = false
  mappings.on_attach(client, bufnr)
end

local commands = {
  OrganizeImports = {
    organize_imports,
    description = "typescript organize imports",
  }
}

lspconfig.tsserver.setup({
  capabilities = lsp_capabilities,
  on_attach = on_attach,
  commands = commands,
  settings = {
    completions = {
      completeFunctionCalls = true
    },
  },
})

