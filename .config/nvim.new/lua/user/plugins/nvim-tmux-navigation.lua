local Plugin = { 'alexghergh/nvim-tmux-navigation' }
local utils = require('utils')

Plugin.config = function()

  local p = require("nvim-tmux-navigation")

  p.setup {
    disable_when_zoomed = true,
    keybindings = {
      left = "<C-h>",
      right = "<C-l>",
      up = "<C-k>",
      down = "<C-j>",
    },
  }
end

return Plugin
