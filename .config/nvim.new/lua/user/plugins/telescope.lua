local Plugin = {'nvim-telescope/telescope.nvim'}
local utils = require("utils")

Plugin.branch = '0.1.x'

Plugin.dependencies = {
  {'nvim-lua/plenary.nvim'},
  {'nvim-telescope/telescope-fzf-native.nvim', build = 'make'},
  {'nvim-telescope/telescope-ui-select.nvim'},
  {'nvim-telescope/telescope-file-browser.nvim'},
}

Plugin.cmd = {'Telescope'}

Plugin.opts = {
  defaults = {
    sorting_strategy = 'ascending',

    initial_mode = "insert",
    file_ignore_patterns = {'tags'},

    mappings = {
      i = {
        ["<cr>"] = 'select_default',
        ["<esc>"] = 'close',
      },
    },

    layout_strategy = "center",
    layout_config = {
      width = 0.9,
      height = 0.6,
    },
  },
  pickers = {
    find_files = {
      -- theme = "dropdown",
    },
    live_grep = {
      -- heme = "dropdown",
    },
    diagnostics = {
      -- heme = "dropdown",
    },
  },
}

function Plugin.init()
  -- See :help telescope.builtin
  local opts = {noremap = true, silent = true}

  utils.nkeymap("<leader>lf", [[<cmd>Telescope current_buffer_fuzzy_find<cr>]], opts)
  utils.nkeymap("<leader>ls", [[<cmd>Telescope find_files<cr>]], opts)
  utils.nkeymap("<leader>lr", [[<cmd>Telescope live_grep<cr>]], opts)
  utils.nkeymap("<leader>lbf", [[<cmd>Telescope buffers<cr>]], opts)
  utils.nkeymap("<leader>lo", [[<cmd>Telescope oldfiles<cr>]], opts)
  utils.nkeymap("<leader>lt", [[<cmd>Telescope tags<cr>]], opts)
  utils.nkeymap("<leader>ld", [[<cmd>Telescope file_browser<cr>]], opts)
  utils.nkeymap("<leader>lgc", [[<cmd>Telescope git_commits<cr>]], opts)
  utils.nkeymap("<leader>lgf", [[<cmd>Telescope git_files<cr>]], opts)
  utils.nkeymap("<leader>lgt", [[<cmd>Telescope lsp_dynamic_workspace_symbols<cr>]], opts)

  utils.nkeymap("gd", [[<cmd>Telescope lsp_definitions<cr>]], opts)
  utils.nkeymap("gi", [[<cmd>Telescope lsp_implementations<cr>]], opts)
  utils.nkeymap("gr", [[<cmd>Telescope lsp_references<cr>]], opts)
  utils.nkeymap("<leader>cd", [[<cmd>Telescope diagnostics<cr>]], opts)
end

function Plugin.config(name, opts)
  local telescope = require('telescope')

  telescope.load_extension('fzf')
  telescope.load_extension('ui-select')

  telescope.setup(opts)
end

return Plugin
