local Plugin = {'nvim-treesitter/nvim-treesitter'}

Plugin.dependencies = {
  {'nvim-treesitter/nvim-treesitter-textobjects'}
}

-- See :help nvim-treesitter-modules
Plugin.opts = {
  highlight = {
    enable = true,
  },
  -- :help nvim-treesitter-textobjects-modules
  textobjects = {
    select = {
      enable = true,
      lookahead = true,
      keymaps = {
        ["ac"] = "@comment.outer"    ,
        ["ic"] = "@class.inner"      ,
        ["ab"] = "@block.outer"      ,
        ["ib"] = "@block.inner"      ,
        ["af"] = "@function.outer"   ,
        ["if"] = "@function.inner"   ,
        -- Leader mappings, dups for whichkey
        ["<Leader><Leader>ab"] = "@block.outer"      ,
        ["<Leader><Leader>ib"] = "@block.inner"      ,
        ["<Leader><Leader>af"] = "@function.outer"   ,
        ["<Leader><Leader>if"] = "@function.inner"   ,
        ["<Leader><Leader>ao"] = "@class.outer"      ,
        ["<Leader><Leader>io"] = "@class.inner"      ,
        ["<Leader><Leader>aC"] = "@call.outer"       ,
        ["<Leader><Leader>iC"] = "@call.inner"       ,
        ["<Leader><Leader>ac"] = "@conditional.outer",
        ["<Leader><Leader>ic"] = "@conditional.inner",
        ["<Leader><Leader>al"] = "@loop.outer"       ,
        ["<Leader><Leader>il"] = "@loop.inner"       ,
        ["<Leader><Leader>ap"] = "@parameter.outer"  ,
        ["<Leader><Leader>ip"] = "@parameter.inner"  ,
        ["<Leader><Leader>is"] = "@scopename.inner"  ,
        ["<Leader><Leader>as"] = "@statement.outer"  ,
      },
    },
  },
  ensure_installed = {
      "bash",
      "c",
      "cpp",
      "css",
      "dockerfile",
      "fish",
      "haskell",
      "html",
      "java",
      "javascript",
      "jsdoc",
      "json",
      "jsonc",
      "kotlin",
      "lua",
      "python",
      "query",
      "rust",
      "scala",
      "toml",
      "tsx",
      "typescript",
      "yaml",
      'vim',
      'vimdoc',
  },
}

function Plugin.config(name, opts)
  require('nvim-treesitter.configs').setup(opts)
end

return Plugin
