vim.cmd([[
  set encoding=utf-8
  set dictionary+=/usr/share/dict/words
  set cc=120
  set colorcolumn=+1
  highlight ColorColumn ctermbg=6
]])

vim.opt.scrolloff=999
vim.opt.sidescrolloff=5
vim.opt.listchars= { eol = '$',tab = '¦·',trail= '·',extends = '>',precedes = '<' }
vim.opt.backspace={"indent", "eol", "start"}
vim.opt.list = true
vim.opt.tabstop=2 -- One tab equal to 4 spaces
vim.opt.shiftwidth=2 -- Indent key will shift 4 spaces
vim.opt.softtabstop=2 -- Tab in insert mode will insert 4 spaces
vim.opt.autochdir = false
vim.opt.incsearch = true
vim.opt.smartcase = true
vim.opt.ignorecase = true
vim.opt.cursorline = false
vim.opt.autoread = true
vim.opt.backup = false
vim.opt.swapfile = false
vim.opt.expandtab = true
vim.opt.modeline = false

--  Do not fail a command. Ask the user
vim.opt.confirm = true

vim.opt.smartindent = true
vim.opt.hidden = true
vim.opt.smarttab = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.showmatch = true
vim.opt.wildmode={"longest", "list", "full"}
vim.opt.wildmenu = true
vim.opt.wildignore= { '*.o','*~','*.bak','*.class','*.out','.git*'}
vim.opt.wrap = false
vim.opt.title = true
vim.opt.showcmd = true
vim.opt.foldlevel=99
-- vim.opt.dictionary.extends('/usr/share/dict/words')
vim.opt.termguicolors = true
vim.opt.inccommand="nosplit"

--  Highlight 120 columns
vim.opt.completeopt={"menu", "menuone", "preview", "noselect", "noinsert"}
-- highlight ColorColumn ctermbg=6

vim.opt.lazyredraw = true
vim.opt.laststatus=2

vim.opt.spelllang = 'en_au'
vim.opt.spell = true

--  Delete all spaces at the end of line
vim.api.nvim_create_autocmd({"BufWritePre"}, {
  pattern = {"*"},
  command = [[%s/\s\+$//e]],
})

--  Use relativenumber in insert mode
--  autocmd InsertEnter * :vim.opt.relativenumber
--  autocmd InsertLeave * :vim.opt.norelativenumber

vim.cmd( "source " .. vim.fn.stdpath("config") .. "/statusline.vim")

