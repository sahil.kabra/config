"===================[ config ]===================================================
let g:vim_config = $HOME . "/.config/nvim/"

let g:ale_completion_enabled = 1

let mapleader=','
map , <leader>

let s:modules = [
    \"plugins",
    \"settings",
    \"mappings",
    \]

for s:module in s:modules
    execute "source" g:vim_config . s:module . ".vim"
endfor

" lua << EOF
"  require 'main'
" EOF

colorscheme codefocus
