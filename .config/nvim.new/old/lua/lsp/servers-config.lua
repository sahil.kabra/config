local lspconfig = require 'lspconfig'
local on_attach = require 'lsp/mappings'.on_attach
local tsserver_config = require'lsp/servers/tsserver'
-- local efm_config = require'lsp/servers/efm'
local null_ls_config = require'lsp/servers/null-ls'
local null_ls = require'null-ls'
local yaml_config = require'lsp/servers/yaml'

local capabilities = require('cmp_nvim_lsp').default_capabilities()

local servers = {"pyright", "jsonls", "rust_analyzer", "bashls", "als", "terraformls"}
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

--lspconfig.lemminx.setup {
--    on_attach = on_attach,
--    capabilities = capabilities,
--    cmd = { "lemminx" },
--}

--lspconfig.hls.setup {
--    on_attach = on_attach,
--    capabilities = capabilities,
--    settings = {
--      languageServerHaskell = {
--        formattingProvider = "fourmolu"
--      }
--    }
--}

lspconfig.tsserver.setup {
    on_attach = tsserver_config.on_attach,
    capabilities = capabilities,
    commands = tsserver_config.commands,
}

lspconfig.yamlls.setup {
    on_attach = yaml_config.on_attach,
    capabilities = capabilities,
    settings = yaml_config.settings,
}

null_ls.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    sources = null_ls_config.sources,
}

lspconfig.omnisharp.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  cmd = { "omnisharp", "--languageserver" },
}
