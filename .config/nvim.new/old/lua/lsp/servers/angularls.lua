local mappings = require 'lsp/mappings'

local nodenv_path = "/home/sahil-kabra_skyfii/.nodenv/versions/14.17.0/lib"
local cmd = {"ngserver", "--stdio", "--tsProbeLocations", nodenv_path , "--ngProbeLocations", nodenv_path}

require'lspconfig'.angularls.setup{
  cmd = cmd,
  on_attach = mappings.on_attach,
  on_new_config = function(new_config, _)
    new_config.cmd = cmd
  end,
}


