local mappings = require 'lsp/mappings'

local eslint = {
  lintCommand = "./node_modules/.bin/eslint -f visualstudio --stdin --stdin-filename ${INPUT}",
  lintStdin = true,
  lintFormats = {"%f:%l:%c: %m"},
  lintIgnoreExitCode = true,
  formatStdin = false
}

local prettier =  {
  formatCommand = "./node_modules/.bin/prettier --stdin --stdin-filepath ${INPUT}",
  formatStdin = true
}

local formatter = prettier
local linter = eslint

local languages = {
  typescript = {formatter, linter},
  javascript = {formatter, linter},
  typescriptreact = {formatter, linter},
  ['typescript.tsx'] = {formatter, linter},
  javascriptreact = {formatter, linter},
  ['javascript.jsx'] = {formatter, linter},
  --yaml = {formatter},
  --json = {formatter},
  html = {formatter},
  scss = {formatter},
  css = {formatter},
  markdown = {formatter},
}

local config = {}

config.settings = {
  rootMarkers = {"package.json",  ".git"},
  languages = languages,
  lintDebounce = 500,
}

config.filetypes = vim.tbl_keys(languages)

config.on_attach = function(client, bufnr)
  client.server_capabilities.hover = false
  client.server_capabilities.rename = false
  client.server_capabilities.completion = false
  client.server_capabilities.goto_definition = false
  client.server_capabilities.document_formatting = true
  mappings.on_attach(client, bufnr)
end

config.init_options = {
  documentFormatting = true,
  codeAction = true,
  completion = true,
}

return config
