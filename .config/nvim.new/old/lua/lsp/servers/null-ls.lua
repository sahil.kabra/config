-- local utils = require('utils')
-- local mappings = require 'lsp/mappings'
local null_ls = require('null-ls')

local config = {}

config.sources = {
  null_ls.builtins.formatting.prettier.with({
    prefer_local = "node_modules/.bin",
  }),
  null_ls.builtins.code_actions.eslint.with({
    prefer_local = "node_modules/.bin",
    extra_args = { "-c", "/home/sahil-kabra_skyfii/.config/eslint/hq/eslintrc.json" },
  }),
  -- null_ls.builtins.diagnostics.eslint.with({
  --   prefer_local = "node_modules/.bin",
    -- extra_args = { "-c", "/home/sahil-kabra_skyfii/.config/eslint/hq/eslintrc.json" },
  -- }),
--  null_ls.builtins.formatting.scalafmt,
--  null_ls.builtins.formatting.terraform_fmt,
--  null_ls.builtins.formatting.fish_indent,
--  null_ls.builtins.formatting.rustfmt,
--  null_ls.builtins.formatting.luacheck,
}

-- local.on_attach = function(client, buffer)
--
return config
