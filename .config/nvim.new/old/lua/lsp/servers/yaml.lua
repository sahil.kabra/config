local mappings = require 'lsp/mappings'

local config = {}

config.on_attach = function(client, bufnr)
  client.server_capabilities.document_formatting = false
  mappings.on_attach(client, bufnr)
end

config.settings = {
  yaml = {
    trace = { server = "verbose" },
    schemas = {
      ["https://raw.githubusercontent.com/OAI/OpenAPI-Specification/main/schemas/v3.0/schema.yaml"] = "/skyfii-api.yml",
    },
    schemaStore = {
      url = "https://www.schemastore.org/api/json/catalog.json",
      enable = true,
    }
  }
}

return config

