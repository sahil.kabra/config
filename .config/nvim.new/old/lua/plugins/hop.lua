local hop = require("hop")
local utils = require("utils")

hop.setup()
utils.nkeymap("<leader>/", "<cmd>lua require'hop'.hint_patterns()<cr>")
