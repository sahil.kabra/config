local cmd = vim.cmd
local mappings = require 'lsp/mappings'

-- vim.opt_global.shortmess:remove("F"):append("c")
cmd 'au BufRead,BufNewFile *.sbt,*.sc set filetype=scala'

cmd [[augroup lsp]]
cmd [[au!]]
cmd [[au FileType scala,sbt lua require("metals").initialize_or_attach(Metals_config)]]
cmd [[augroup end]]

cmd([[hi! link LspReferenceText CursorColumn]])
cmd([[hi! link LspReferenceRead CursorColumn]])
cmd([[hi! link LspReferenceWrite CursorColumn]])

Metals_config = require("metals").bare_config()

local capabilities = require('cmp_nvim_lsp').default_capabilities()

Metals_config.settings = {
  showImplicitArguments = true,
  showImplicitConversionsAndClasses = true,
  showInferredType = true,
  excludedPackages = {"akka.actor.typed.javadsl", "com.github.swagger.akka.javadsl"},
  disabledMode = true,
  useGlobalExecutable = true,
}

Metals_config.on_attach = function(client, buffer)
  require("metals").setup_dap()
  mappings.on_attach(client, buffer)
end
Metals_config.capabilities = capabilities
Metals_config.root_patterns = {'build.sbt', 'pom.xml'}

