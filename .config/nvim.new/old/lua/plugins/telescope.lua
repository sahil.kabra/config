local actions = require('telescope.actions')
local utils = require('utils')

require('telescope').setup {
  defaults = {
    file_sorter = require('telescope.sorters').get_fzy_sorter,
    sorting_strategy = 'ascending',

    initial_mode = 'insert',
    file_ignore_patterns = {"tags"},

    --file_previewer = require('telescope.previewers').vim_buffer_cat.new,
    --grep_previewer = require('telescope.previewers').vim_buffer_vimgrep.new,
    --qflist_previewer = require('telescope.previewers').vim_buffer_qflist.new,

    mappings = {
      i = {
        ["<cr>"] = actions.select_default,
        ["<esc>"] = actions.close,
        --["<c-q>"] = actions.send_to_qflist,
      },
    },
  }
}

require('telescope').load_extension('fzy_native')
require('telescope').load_extension('ui-select')

local opts = {noremap = true, silent = true}

utils.nkeymap("<leader>lf", [[<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<cr>]], opts)
utils.nkeymap("<leader>ls", [[<cmd>lua require('telescope.builtin').find_files()<cr>]], opts)
utils.nkeymap("<leader>lr", [[<cmd>lua require('telescope.builtin').live_grep()<cr>]], opts)
utils.nkeymap("<leader>lb", [[<cmd>lua require('telescope.builtin').buffers()<cr>]], opts)
utils.nkeymap("<leader>lo", [[<cmd>lua require('telescope.builtin').oldfiles()<cr>]], opts)
utils.nkeymap("<leader>lt", [[<cmd>lua require('telescope.builtin').tags()<cr>]], opts)
utils.nkeymap("<leader>ld", [[<cmd>lua require('telescope.builtin').file_browser()<cr>]], opts)
utils.nkeymap("<leader>ntt", [[<cmd>lua require('telescope.builtin').file_browser()<cr>]], opts)
utils.nkeymap("<leader>lgc", [[<cmd>lua require('telescope.builtin').git_commits()<cr>]], opts)
utils.nkeymap("<leader>lgf", [[<cmd>lua require('telescope.builtin').git_files()<cr>]], opts)
utils.nkeymap("<leader>lgt", [[<cmd>lua require('telescope.builtin').lsp_dynamic_workspace_symbols()<cr>]], opts)

utils.nkeymap("gd", [[<cmd>lua require('telescope.builtin').lsp_definitions()<cr>]], opts)
utils.nkeymap("gi", [[<cmd>lua require('telescope.builtin').lsp_implementations()<cr>]], opts)
utils.nkeymap("gr", [[<cmd>lua require('telescope.builtin').lsp_references()<cr>]], opts)
utils.nkeymap("<leader>cd", [[<cmd>lua require('telescope.builtin').diagnostics()<cr>]], opts)
