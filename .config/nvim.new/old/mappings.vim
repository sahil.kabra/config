" Fast Saving
nnoremap <leader>fs :w!<cr>

nnoremap <Space> :
vnoremap <Space> :
nnoremap <leader>rc :source $MYVIMRC<cr>
nnoremap <leader>e :e $MYVIMRC<cr>

inoremap <C-c> <Esc>
" toggle folds
nnoremap zt za

nnoremap dg :diffget<cr>
nnoremap dp :diffput<cr>

" toggle relative number
nnoremap rnu :set rnu!<CR>

" Moving between splits
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Resize splits
nnoremap <leader>vrp :vertical resize +5<cr>
nnoremap <leader>vrm :vertical resize -5<cr>
nnoremap <leader>vru :resize +5<cr>
nnoremap <leader>vrd :resize -5<cr>

" Split line at cursor
nnoremap <leader>cr i<CR><Esc>
nnoremap <leader>ncr J

nnoremap gb <C-o>

nnoremap <leader>gg :vertical G<cr>

" easier to type
noremap H ^

" Better movement
nnoremap <tab> %
vnoremap <tab> %

" toggle search highlighting
nnoremap <leader>hls :set hlsearch!<CR>

" tagbar
nmap <silent> tt :TagbarToggle<CR>

" easymotion
nmap <leader>/ <Plug>(easymotion-sn)
nmap <leader>n <Plug>(easymotion-next)
nmap <leader>p <Plug>(easymotion-prev)

nmap <leader>ga <Plug>(EasyAlign)
xmap <leader>ga <Plug>(EasyAlign)

map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

function! ToggleWindowHorizontalVerticalSplit()
  if !exists('t:splitType')
    let t:splitType = 'vertical'
  endif

  if t:splitType == 'vertical' " is vertical switch to horizontal
    windo wincmd K
    let t:splitType = 'horizontal'

  else " is horizontal switch to vertical
    windo wincmd H
    let t:splitType = 'vertical'
  endif
endfunction

nnoremap <silent> <leader>wt :call ToggleWindowHorizontalVerticalSplit()<cr>

ino <silent><expr> <C-f> pumvisible() ? (complete_info().selected == -1 ? "\<C-e><CR>" : "\<C-y>") : "\<C-f>"

" goyo mappings
nnoremap <leader>mdfc :Goyo 200+50x95%<cr>
nnoremap <leader>mdft :Goyo 300x95%<cr>
