#!/bin/bash

npm install -g \
  neovim \
  typescript typescript-language-server \
  pyright \
  @angular/language-server \
  diagnostic-languageserver \
  dockerfile-language-server-nodejs \
  vscode-langservers-extracted \
  yarn \
  prettier \
  yaml-language-server \
  bash-language-server
