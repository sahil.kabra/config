" colorscheme PaperColor
" colorscheme nofrils-light

" use surround mappings for sandwich plugin
runtime macros/sandwich/keymap/surround.vim

" enable matching for if/else/html/xml etc
runtime plugin/matchit.vim

"autocmd FileType typescript :TSStart
colorscheme codefocus

"autocmd VimEnter * COQnow -s
