" Maintainer: sahil kabra

set background=light
hi clear
if exists('syntax_on')
  syntax reset
endif
let g:colors_name='codefocus'

hi Normal guifg=#000000 ctermfg=16 guibg=#e4e4e4 ctermbg=254 gui=NONE cterm=NONE
hi Comment guifg=#808080 ctermfg=244 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi Constant guifg=#262626 ctermfg=235 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi String guifg=#585858 ctermfg=240 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi Identifier guifg=#585858 ctermfg=240 guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi Function guifg=#585858 ctermfg=240 guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi Statement guifg=#585858 ctermfg=240 guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi PreProc guifg=#262626 ctermfg=235 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Title guifg=#080808 ctermfg=232 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Type guifg=#262626 ctermfg=235 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Special guifg=#262626 ctermfg=235 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Delimiter guifg=#262626 ctermfg=235 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Underlined guifg=#585858 ctermfg=240 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi ColorColumn guifg=NONE ctermfg=NONE guibg=#dadada ctermbg=253 gui=NONE cterm=NONE
hi Conceal guifg=#8a8a8a ctermfg=245 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi Directory guifg=#808080 ctermfg=244 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffAdd guifg=#0e6655 ctermfg=23 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffChange guifg=#b7950b ctermfg=136 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffDelete guifg=#d16969 ctermfg=167 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffText guifg=#b7950b ctermfg=136 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi LineNr guifg=#bcbcbc ctermfg=250 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CursorLineNr guifg=#585858 ctermfg=240 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NonText guifg=#d0d0d0 ctermfg=252 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Question guifg=#080808 ctermfg=232 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Search guifg=#000000 ctermfg=16 guibg=#dadada ctermbg=253 gui=bold cterm=bold
hi Visual guifg=NONE ctermfg=NONE guibg=#ffffd7 ctermbg=230 gui=NONE cterm=NONE
hi WarningMsg guifg=#080808 ctermfg=232 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi MatchParen guifg=NONE ctermfg=NONE guibg=#b7950b ctermbg=136 gui=NONE cterm=NONE
hi MoreMsg guifg=#080808 ctermfg=232 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Error guifg=#d16969 ctermfg=167 guibg=NONE ctermbg=NONE gui=bold,reverse cterm=bold,reverse
hi Pmenu guifg=#000000 ctermfg=16 guibg=#ffffd7 ctermbg=230 gui=NONE cterm=NONE
hi PmenuSel guifg=#000000 ctermfg=16 guibg=#ffd7ff ctermbg=225 gui=NONE cterm=NONE
hi PmenuSbar guifg=NONE ctermfg=NONE guibg=#262626 ctermbg=235 gui=NONE cterm=NONE
hi PmenuThumb guifg=NONE ctermfg=NONE guibg=#ffffff ctermbg=231 gui=NONE cterm=NONE
hi Todo guifg=#c586c0 ctermfg=175 guibg=NONE ctermbg=NONE gui=bold,italic cterm=bold,italic
hi Float guifg=#569cd6 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownCode guifg=NONE ctermfg=NONE guibg=#dadada ctermbg=253 gui=NONE cterm=NONE
hi markdownCodeBlock guifg=NONE ctermfg=NONE guibg=#dadada ctermbg=253 gui=NONE cterm=NONE
hi markdownCodeDelimiter guifg=NONE ctermfg=NONE guibg=#dadada ctermbg=253 gui=NONE cterm=NONE
hi markdownH1 guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi markdownH2 guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi markdownH3 guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi markdownH4 guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi markdownH5 guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi markdownH6 guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi markdownUrl guifg=#569cd6 ctermfg=74 guibg=NONE ctermbg=NONE gui=underline cterm=underline
