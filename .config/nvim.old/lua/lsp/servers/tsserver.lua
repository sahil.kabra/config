local mappings = require 'lsp/mappings'

local function organize_imports()
  local p = {
    command = "_typescript.organizeImports",
    arguments = {vim.api.nvim_buf_get_name(0)},
    title = ""
  }

  vim.lsp.buf.execute_command(p)
end

local config = {}

config.on_attach = function(client, bufnr)
  client.server_capabilities.document_formatting = false
  mappings.on_attach(client, bufnr)
end

config.commands = {
  OrganizeImports = {
    organize_imports,
    description = "typescript organize imports",
  }
}

return config
