-- plugin config
require 'plugins/telescope'
require 'plugins/treesitter'
require 'plugins/nvim-tree'
require 'plugins/nvim-cmp'
require 'plugins/hop'
require 'colorizer'.setup()
--require 'plugins/jdtls'
require 'plugins/no-neck-pain'

-- lsp
require 'lsp/servers-config'
require 'lsp/diagnostic-config'
require 'lsp/servers/lspinstall'

require 'plugins/metals'
require 'plugins/copilot'
