local utils = require "utils"

vim.opt_global.completeopt = {"menuone", "noinsert", "noselect"}

require 'compe'.setup {
  autocomplete = true;
  debug = false;
  documentation = true;
  enabled = true;
  incomplete_delay = 400;
  max_abbr_width = 100;
  max_kind_width = 100;
  max_menu_width = 100;
  min_length = 1;
  preselect = 'enable';
  source_timeout = 200;
  throttle_time = 80;

  source = {
    buffer = true;
    calc = true;
    nvim_lsp = true;
    nvim_lua = true;
    path = true;
    spell = true;
    tags = true;
    treesitter = true;
    vsnip = true;
    tmux = {
      disabled = false,
      all_panes = true,
      kind = 'Text',
    };
  };
}

utils.ikeymap("<C-f>", "compe#confirm('<C-y>', 'i')", {expr = true})
