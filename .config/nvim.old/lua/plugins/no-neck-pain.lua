local no_neck_pain = require("no-neck-pain")

no_neck_pain.setup({
  debug = false,

  buffers = {
    setNames = false,
  },

  -- buffer scoped options
  bo = {
  },
  -- window scoped options
  wo = {
    number = true,
    relativeNumber = true,
  },

  integrations = {
    NvimTree = {
      position = "left",
    }
  },
})
