local utils = require "utils"

vim.opt_global.completeopt = {"menu", "menuone", "noselect"}

local cmp = require 'cmp'

cmp.setup {
  snippet = {
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body)
      end,
    },
  mapping = cmp.mapping.preset.insert({
    ['<C-j>'] = cmp.mapping.scroll_docs(-4),
    ['<C-k>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<C-f>'] = cmp.mapping.confirm({ select = true }),
  }),
  sources = {
    { name = 'copilot'},
    { name = 'nvim_lsp'},
    { name = 'buffer' },
    { name = 'path' },
    { name = 'treesitter' },
    { name = 'spell' },
    { name = 'tags' },
    { name = 'tmux'},
  },
  formatting = {
    fields = { "abbr", "menu" }
  }
}
