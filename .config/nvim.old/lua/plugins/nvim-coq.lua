vim.g.coq_settings = {
  keymap = {
    recommended = true,
    jump_to_mark = ']h',
  }
}
