local utils = require('utils')

utils.nkeymap("<leader>ntt", [[<cmd>NvimTreeToggle<cr>]])
utils.nkeymap("<leader>ntr", [[<cmd>NvimTreeRefresh<cr>]])
utils.nkeymap("<leader>ntf", [[<cmd>NvimTreeFindFile<cr>]])

require'nvim-tree'.setup {
      disable_netrw       = true,
      hijack_netrw        = true,
      open_on_tab         = false,
      hijack_cursor       = false,
      update_cwd          = false,
      update_focused_file = {
        enable      = false,
        update_cwd  = false,
        ignore_list = {}
      },
      system_open = {
        cmd  = nil,
        args = {}
      },
      git = {
        enable = false,
        ignore = false,
        timeout = 500,
      },
      view = {
        width = 30,
        side = 'left',
        adaptive_size = true
      },
      renderer = {
        add_trailing = true,
        group_empty = true,
        icons = {
          show = {
            git = false,
            folder = false,
            folder_arrow = false,
            file = false,
          },
          glyphs = {
            default = '',
            symlink = '',
            folder = {
              arrow_open = "",
              arrow_closed = "",
              default = "+",
              open = "-",
              empty = "-",
              empty_open = "-",
              symlink = "+",
              symlink_open = "-",
            },
          }
        }
      }
    }

