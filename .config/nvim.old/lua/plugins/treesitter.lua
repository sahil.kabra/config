local treesitter = require('nvim-treesitter.configs')

vim.wo.foldmethod = 'expr'
vim.wo.foldexpr = 'nvim_treesitter#foldexpr()'

treesitter.setup {
  ensure_installed = {
      "bash",
      "c",
      "cpp",
      "css",
      "dockerfile",
      "fish",
      "haskell",
      "html",
      "java",
      "javascript",
      "jsdoc",
      "json",
      "jsonc",
      "kotlin",
      "lua",
      "python",
      "query",
      "rust",
      "scala",
      "toml",
      "tsx",
      "typescript",
      "yaml",
  },
  highlight   = {
    enable = true,
  },
  indent = { enable = true, },
  textobjects = {
    select = {
      enable  = true,
      keymaps = {
        ["ac"] = "@comment.outer"      ,
        ["ic"] = "@class.inner"      ,
        ["ab"] = "@block.outer"      ,
        ["ib"] = "@block.inner"      ,
        ["af"] = "@function.outer"   ,
        ["if"] = "@function.inner"   ,
        -- Leader mappings, dups for whichkey
        ["<Leader><Leader>ab"] = "@block.outer"      ,
        ["<Leader><Leader>ib"] = "@block.inner"      ,
        ["<Leader><Leader>af"] = "@function.outer"   ,
        ["<Leader><Leader>if"] = "@function.inner"   ,
        ["<Leader><Leader>ao"] = "@class.outer"      ,
        ["<Leader><Leader>io"] = "@class.inner"      ,
        ["<Leader><Leader>aC"] = "@call.outer"       ,
        ["<Leader><Leader>iC"] = "@call.inner"       ,
        ["<Leader><Leader>ac"] = "@conditional.outer",
        ["<Leader><Leader>ic"] = "@conditional.inner",
        ["<Leader><Leader>al"] = "@loop.outer"       ,
        ["<Leader><Leader>il"] = "@loop.inner"       ,
        ["<Leader><Leader>ap"] = "@parameter.outer"  ,
        ["<Leader><Leader>ip"] = "@parameter.inner"  ,
        ["<Leader><Leader>is"] = "@scopename.inner"  ,
        ["<Leader><Leader>as"] = "@statement.outer"  ,
      },
    },
  },
  playground = {
    enable = true,
    disable = {},
    updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
    persist_queries = false, -- Whether the query persists across vim sessions
    keybindings = {
      toggle_query_editor = 'o',
      toggle_hl_groups = 'i',
      toggle_injected_languages = 't',
      toggle_anonymous_nodes = 'a',
      toggle_language_display = 'I',
      focus_language = 'f',
      unfocus_language = 'F',
      update = 'R',
      goto_node = '<cr>',
      show_help = '?',
    },
  },
}

if pcall(require, "nvim-treesitter.parsers") then
  -- install with ':TSInstallSync markdown'
  require "nvim-treesitter.parsers".get_parser_configs().markdown = {
    install_info = {
      url = "https://github.com/ikatyang/tree-sitter-markdown",
      files = { "src/parser.c", "src/scanner.cc" },
    }
  }
end
