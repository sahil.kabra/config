local M = {}

function M.keymap(mode, lhs, rhs, opts)
  return vim.api.nvim_set_keymap(mode, lhs, rhs, vim.tbl_extend('keep', opts or {}, {
        nowait = true,
        silent = false,
        noremap = true,
    }))
end

function M.nkeymap(...) M.keymap('n', ...) end
function M.ikeymap(...) M.keymap('i', ...) end

function M.buf_keymap(buf, mode, lhs, rhs, opts)
  return vim.api.nvim_buf_set_keymap(buf, mode, lhs, rhs, vim.tbl_extend('keep', opts or {}, {
        nowait = true,
        silent = true,
        noremap = true,
    }))
end

function M.buf_option(buf, option, value)
  return vim.api.nvim_buf_set_option(buf, option, value)
end

function M.command(cmd)
  return vim.api.nvim_command(cmd)
end

function M.get_project_name()
  return vim.fn.fnamemodify(vim.fn.getcwd(), ':p:h:t')
end

return M
