let g:tagbar_type_typescript = {
  \ 'ctagstype': 'typescript',
  \ 'kinds': [
    \ 'c:classes',
    \ 'n:modules',
    \ 'f:functions',
    \ 'v:variables',
    \ 'v:varlambdas',
    \ 'm:members',
    \ 'i:interfaces',
    \ 'e:enums',
  \ ]
\ }

let g:tagbar_type_javascript = {
  \ 'ctagstype': 'javascript',
  \ 'kinds': [
    \ 'A:Arrays',
    \ 'C:Classes',
    \ 'E:Export',
    \ 'F:Functions',
    \ 'G:Generators',
    \ 'I:Imports',
    \ 'M:Methods',
    \ 'P:Properties',
    \ 'O:Objects',
    \ 'T:Tags',
    \ 'V:Variables',
  \ ]
\}
