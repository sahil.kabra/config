let s:plug_dir = g:vim_config . "plugged"
let s:plugin_settings_dir = g:vim_config . "startup/plugins"

call plug#begin(s:plug_dir)

"-------------------[ interface ]------------------------------------------------
Plug 'will133/vim-dirdiff'

" easily align stuff
Plug 'junegunn/vim-easy-align'

" vim folding
Plug 'tmhedberg/SimpylFold'

" background tag generation
Plug 'https://github.com/ludovicchabant/vim-gutentags'
if executable('rg')
  let g:gutentags_file_list_command = 'rg --files'
endif

Plug 'NLKNguyen/papercolor-theme'

Plug 'editorconfig/editorconfig-vim'

"-------------------[ tools ]------------------------------------------------
" show tags on the side
Plug 'preservim/tagbar'
" Plug 'easymotion/vim-easymotion'

" align code
"Plug 'godlygeek/tabular'

" plugin repeat
Plug 'tpope/vim-repeat'

" plugin to set buffer options
Plug 'tpope/vim-sleuth'

Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-path'
Plug 'f3fora/cmp-spell'
Plug 'quangnguyen30192/cmp-nvim-tags'
Plug 'ray-x/cmp-treesitter'
Plug 'andersevenrud/compe-tmux'
" needed snip plugins otherwise getting error
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/nvim-cmp'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
Plug 'nvim-lua/popup.nvim'

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', {'tag': '0.1.6'}
Plug 'nvim-telescope/telescope-ui-select.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim'

Plug 'scalameta/nvim-metals'
Plug 'sindrets/diffview.nvim'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'kyazdani42/nvim-tree.lua'
Plug 'phaazon/hop.nvim'
Plug 'mfussenegger/nvim-dap'
Plug 'mfussenegger/nvim-jdtls'
Plug 'jose-elias-alvarez/null-ls.nvim'
Plug 'shortcuts/no-neck-pain.nvim', { 'tag': '*' }
"Plug 'github/copilot.vim'
Plug 'zbirenbaum/copilot.lua'
Plug 'zbirenbaum/copilot-cmp'

"Plug 'junegunn/goyo.vim'
"Plug 'stevearc/aerial.nvim'
"Plug 'nvim-treesitter/nvim-treesitter-angular'
"Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
" snippets
"Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}
"Plug 'nvim-treesitter/playground'

Plug 'hashivim/vim-terraform'
"-------------------[ commands ]------------------------------------------------
" Toggle commment
Plug 'tomtom/tcomment_vim'

Plug 'tpope/vim-surround'

"-------------------[ tmux ]------------------------------------------------
Plug 'christoomey/vim-tmux-navigator'

"-------------------[ language ]------------------------------------------------
" Git integration
Plug 'tpope/vim-fugitive'

"-------------------( json )------------------------------------------------
Plug 'kevinoid/vim-jsonc', {'for': 'json'}

"-------------------( markdown )------------------------------------------------
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
"
"-------------------[ testing ]------------------------------------------------

"-------------------[ commented ]------------------------------------------------

call plug#end()            " required

" If plugin dir is empty, install
if empty(s:plug_dir)
    autocmd! VimEnter * PlugInstall
endif
