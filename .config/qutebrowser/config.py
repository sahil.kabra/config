#import setproctitle
#setproctitle.setproctitle("qutebrowser")

config.load_autoconfig(True)

# Bindings
config.bind("gi", "hint inputs")
config.bind("<f12>", "inspector")
config.bind("gld", "hint links download")

config.unbind("J")
config.bind("J", "tab-prev")

config.unbind("K")
config.bind("K", "tab-next")

config.unbind("+")
config.unbind("-")
config.unbind("=")
config.bind("z+", "zoom-in")
config.bind("z-", "zoom-out")
config.bind("zz", "zoom")

config.unbind("O")
config.unbind("T")
config.unbind("th")
config.unbind("tl")
config.bind("O", "set-cmd-text :open {url:pretty}")
config.bind("T", "set-cmd-text :open -t {url:pretty}")
config.bind("t", "set-cmd-text -s :open -t")

config.unbind("<ctrl+tab>")
config.bind("<ctrl+tab>", "tab-next")
config.bind("<ctrl+shift+tab>", "tab-prev")

config.unbind("ZQ")
config.unbind("ZZ")
config.unbind("<ctrl+q>")
config.bind("<ctrl+q>", "wq")

config.unbind("H")
config.unbind("L")
config.bind("gb", "back")
config.bind("gf", "forward")
config.bind("gh", "history")

config.unbind("d")
config.bind("D", "tab-close")

# Aliases for commands. The keys of the given dictionary are the
# aliases, while the values are the commands they map to.
c.aliases = {
    "q": "quit",
    "w": "session-save",
    "wq": "quit --save",
    "gmail": "open -t https://mail.google.com",
    "drive": "open -t https://drive.google.com",
    "blaze": "open -t https://192.168.1.1:8443",
}

c.downloads.remove_finished = 2000

# Position of new tabs opened from another tab.
# Type: NewTabPosition
# Valid values:
#   - prev: Before the current tab.
#   - next: After the current tab.
#   - first: At the beginning.
#   - last: At the end.
c.tabs.new_position.related = 'next'

# Dont restore open sites when qutebrowser is reopened.
c.auto_save.session = False

# Foreground color of the URL in the statusbar on successful load
# (https).
c.colors.statusbar.url.success.https.fg = "white"

# Open new tabs (middleclick/ctrl+click) in the background.
c.tabs.background = True
c.tabs.padding = {'bottom': 5, 'left': 5, 'right': 5, 'top': 5}
c.tabs.title.alignment = 'center'
c.tabs.title.format = '{index}: {current_title} - {host}'
c.colors.tabs.bar.bg = '#555555'
c.colors.tabs.indicator.start = 'black'
c.colors.tabs.indicator.stop = 'black'
c.colors.tabs.odd.bg = 'lightgrey'
c.colors.tabs.odd.fg = 'black'
c.colors.tabs.even.bg = 'lightgrey'
c.colors.tabs.even.fg = 'black'
c.colors.tabs.selected.odd.bg = 'darkgrey'
c.colors.tabs.selected.odd.fg = 'black'
c.colors.tabs.selected.even.bg = 'darkgrey'
c.colors.tabs.selected.even.fg = 'black'

# The height of the completion, in px or as percentage of the window.
c.completion.height = "30%"

# Move on to the next part when there's only one possible completion
# left.
c.completion.quick = False

# When to show the autocompletion window.
# Valid values:
#   - always: Whenever a completion is available.
#   - auto: Whenever a completion is requested.
#   - never: Never.
c.completion.show = "auto"

c.statusbar.padding = {'bottom': 3, 'left': 2, 'right': 5, 'top': 3}
c.statusbar.position = 'bottom'
c.statusbar.widgets = ["keypress", "url", "scroll", "history", "tabs", "progress"]
c.colors.statusbar.normal.fg = 'black'
c.colors.statusbar.normal.bg = 'darkgrey'
c.colors.statusbar.insert.bg = '#039B9A'
c.colors.statusbar.insert.fg = 'black'
c.colors.statusbar.command.fg = 'black'
c.colors.statusbar.command.bg = 'darkgrey'
c.colors.statusbar.normal.fg = 'black'
c.colors.statusbar.normal.fg = 'black'
c.colors.statusbar.url.fg = 'black'
c.colors.statusbar.url.error.fg = 'orange'
c.colors.statusbar.url.success.http.fg = 'black'

# Whether quitting the application requires a confirmation.
# Valid values:
#   - always: Always show a confirmation.
#   - multiple-tabs: Show a confirmation if multiple tabs are opened.
#   - downloads: Show a confirmation if downloads are running
#   - never: Never show a confirmation.
c.confirm_quit = ["downloads"]

# Value to send in the `Accept-Language` header.
#c.content.headers.accept_language = "en-US,en;q=0.8,fi;q=0.6"

# The proxy to use. In addition to the listed values, you can use a
# `socks://...` or `http://...` URL.
# Valid values:
#   - system: Use the system wide proxy.
#   - none: Don"t use any proxy
c.content.proxy = "none"

# Validate SSL handshakes.
# Valid values:
#   - true
#   - false
#   - ask
c.content.tls.certificate_errors = "ask"

# A list of user stylesheet filenames to use.
#c.content.user_stylesheets = "user.css"

# The directory to save downloads to. If unset, a sensible os-specific
# default is used.
#c.downloads.location.directory = "/tmp/ape"

# Prompt the user for the download location. If set to false,
# `downloads.location.directory` will be used.
c.downloads.location.prompt = False

# The editor (and arguments) to use for the `open-editor` command. `{}`
# gets replaced by the filename of the file to be edited.
#c.editor.command = ["termite", "-e", "vim '{}'"]

# Default monospace fonts. Whenever "monospace" is used in a font
# setting, it's replaced with the fonts listed here.
# Type: Font
monospace = '"Hack, xos4 Terminus", Terminus, Monospace, "DejaVu Sans Mono", Monaco, "Bitstream Vera Sans Mono", "Andale Mono", "Courier New", Courier, "Liberation Mono", monospace, Fixed, Consolas, Terminal'

# Font used in the statusbar.
# Type: Font
c.fonts.statusbar = '10pt monospace'

# Font used in the tab bar.
# Type: QtFont
#c.fonts.tabs = '9pt monospace'

# Font used in the completion categories.
c.fonts.completion.category = f"bold {monospace}"

# Font used in the completion widget.
c.fonts.completion.entry = monospace

# Font used for the debugging console.
c.fonts.debug_console = monospace

# Font used for the downloadbar.
c.fonts.downloads = monospace

# Font used in the keyhint widget.
c.fonts.keyhint = monospace

# Font used for error messages.
c.fonts.messages.error = monospace

# Font used for info messages.
c.fonts.messages.info = monospace

# Font used for warning messages.
c.fonts.messages.warning = monospace

# Font used for prompts.
c.fonts.prompts = monospace

# Font used for the hints.
c.fonts.hints = "bold 13px 'DejaVu Sans Mono'"

# Leave insert mode if a non-editable element is clicked.
c.input.insert_mode.auto_leave = True

# Automatically enter insert mode if an editable element is focused
# after loading the page.
c.input.insert_mode.auto_load = True

# Show a scrollbar.
#c.scrolling.bar = True

# Enable smooth scrolling for web pages. Note smooth scrolling does not
# work with the `:scroll-px` command.
c.scrolling.smooth = False

# Behavior when the last tab is closed.
# Valid values:
#   - ignore: Don't do anything.
#   - blank: Load a blank page.
#   - startpage: Load the start page.
#   - default-page: Load the default page.
#   - close: Close the window.
c.tabs.last_close = "startpage"

# Which tab to select when the focused tab is removed.
# Valid values:
#   - prev: Select the tab which came before the closed one (left in horizontal, above in vertical).
#   - next: Select the tab which came after the closed one (right in horizontal, below in vertical).
#   - last-used: Select the previously selected tab.
c.tabs.select_on_remove = "prev"

# Width of the progress indicator (0 to disable).
#c.tabs.width.indicator = 0

# The page to open if :open -t/-b/-w is used without URL. Use
# `about:blank` for a blank page.
c.url.default_page = "about:blank"

# Definitions of search engines which can be used via the address bar.
# Maps a searchengine name (such as `DEFAULT`, or `ddg`) to a URL with a
# `{}` placeholder. The placeholder will be replaced by the search term,
# use `{{` and `}}` for literal `{`/`}` signs. The searchengine named
# `DEFAULT` is used when `url.auto_search` is turned on and something
# else than a URL was entered to be opened. Other search engines can be
# used by prepending the search engine name to the search term, e.g.
# `:open google qutebrowser`.
c.url.searchengines = {"DEFAULT": "https://www.google.com.au/search?q={}"}

# The page(s) to open at the start.
c.url.start_pages = "https://google.com.au/"

c.window.hide_decoration = False
