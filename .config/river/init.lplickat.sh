#!/bin/bash

logger "Starting river."


##############
# Appearance #
##############
#[[ -e ~/.config/wall.jpg ]] && riverctl spawn "swaybg -m fill -i ~/.config/wall.jpg"
riverctl background-color       0x777777
riverctl border-color-unfocused 0x9EEEEE
riverctl border-color-focused   0x55AAAA
riverctl border-width 3
riverctl xcursor-theme tarazed
gsettings set org.gnome.desktop.interface gtk-theme Adwaita
gsettings set org.gnome.desktop.interface icon-theme Breeze
gsettings set org.gnome.desktop.wm.preferences button-layout " "
gsettings set org.gnome.desktop.interface cursor-theme tarazed


##########
# Inputs #
##########
riverctl input "1149:4128:Kensington_Expert_Mouse" accel-profile none
riverctl input "1149:4128:Kensington_Expert_Mouse" pointer-accel 0.6
riverctl input "1149:4128:Kensington_Expert_Mouse" scroll-method button
riverctl input "1149:4128:Kensington_Expert_Mouse" scroll-button BTN_SIDE
riverctl input "2:10:TPPS/2_IBM_TrackPoint" accel-profile none
riverctl input "2:10:TPPS/2_IBM_TrackPoint" pointer-accel 0.2
riverctl set-repeat 40 300


###########
# Interop #
###########
gsettings set org.gnome.desktop.default-applications.terminal exec foot.desktop
riverctl spawn "systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=river"
riverctl spawn "dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=river"
riverctl spawn /lib/xdg-desktop-portal
riverctl spawn /lib/xdg-desktop-portal-gtk
riverctl spawn /lib/xdg-desktop-portal-wlr


###########
# Widgets #
###########
riverctl spawn mako
riverctl map normal Super Escape spawn "makoctl dismiss"
riverctl spawn river-tag-overlay
riverctl spawn kanshi


####################
# Launchy Keybinds #
####################
riverctl map normal Super       N spawn "foot"
riverctl map normal Super+Shift N spawn "foot --app-id popup"
riverctl map normal Super       D spawn "foot --app-id launcher /home/leon/.config/river/launch.sh"
riverctl map normal Super       B spawn "foot --app-id launcher /home/leon/.config/river/book.sh"
riverctl map normal Super       O spawn "foot --app-id launcher /home/leon/.config/river/online.sh"
riverctl map normal Super       Print spawn "grim"
riverctl map normal Mod4         w spawn 'grim -g "$(slurp)" - | imv -'


#####################
# Window management #
#####################
riverctl map normal Super+Shift        Q      close
riverctl map normal Super              Return zoom
riverctl map normal Super              F      toggle-fullscreen
riverctl map normal Super              Space  toggle-float
riverctl map normal Super              J      focus-view next
riverctl map normal Super              K      focus-view previous
riverctl map normal Super+Shift        J      swap next
riverctl map normal Super+Shift        K      swap previous
riverctl map normal Super+Alt         H      move left 100
riverctl map normal Super+Alt         J      move down 100
riverctl map normal Super+Alt         K      move up 100
riverctl map normal Super+Alt         L      move right 100
riverctl map normal Super+Alt+Control H      snap left
riverctl map normal Super+Alt+Control J      snap down
riverctl map normal Super+Alt+Control K      snap up
riverctl map normal Super+Alt+Control L      snap right
riverctl map normal Super+Alt+Shift   H      resize horizontal -100
riverctl map normal Super+Alt+Shift   J      resize vertical 100
riverctl map normal Super+Alt+Shift   K      resize vertical -100
riverctl map normal Super+Alt+Shift   L      resize horizontal 100
riverctl map normal Super              Backspace focus-output next
riverctl map normal Super+Shift        Backspace send-to-output next
riverctl set-cursor-warp on-output-change


##########
# Layout #
##########
riverctl spawn "stacktile --per-tag-config --drop-empty-configs"
riverctl default-layout stacktile
riverctl map normal Super       H     send-layout-cmd stacktile "primary_ratio -0.05"
riverctl map normal Super       L     send-layout-cmd stacktile "primary_ratio +0.05"
riverctl map normal Super+Shift H     send-layout-cmd stacktile "primary_count  +1"
riverctl map normal Super+Shift L     send-layout-cmd stacktile "primary_count  -1"
riverctl map normal Super       Minus send-layout-cmd stacktile "all_padding -10"
riverctl map normal Super       Plus  send-layout-cmd stacktile "all_padding +10"
riverctl map normal Super       Up    send-layout-cmd stacktile "primary_position top"
riverctl map normal Super       Right send-layout-cmd stacktile "primary_position right"
riverctl map normal Super       Down  send-layout-cmd stacktile "primary_position bottom"
riverctl map normal Super       Left  send-layout-cmd stacktile "primary_position left"
riverctl map normal Super+Alt   Space send-layout-cmd stacktile "primary_sublayout rows,columns,full"
for i in $(seq 1 9)
do
    riverctl map normal Super+Alt $i send-layout-cmd stacktile "primary_count $i"
done


########
# Tags #
########
for i in $(seq 1 9)
do
    tagmask=$((1 << ($i - 1)))
    riverctl map normal Super               $i set-focused-tags    $tagmask
    riverctl map normal Super+Shift         $i set-view-tags       $tagmask
    riverctl map normal Super+Control       $i toggle-focused-tags $tagmask
    riverctl map normal Super+Shift+Control $i toggle-view-tags    $tagmask

    tagmask=$((1 << (($i - 1) + 9)))
    riverctl map normal Super               F$i set-focused-tags    $tagmask
    riverctl map normal Super+Shift         F$i set-view-tags       $tagmask
    riverctl map normal Super+Control       F$i toggle-focused-tags $tagmask
    riverctl map normal Super+Shift+Control F$i toggle-view-tags    $tagmask

    #)) <- needed to make kakounes syntax highlighting work, for whatever unholy reason
done
all_tags_mask=$(((1 << 32) - 1))
riverctl map normal Super       0 set-focused-tags $all_tags_mask
riverctl map normal Super+Shift 0 set-view-tags    $all_tags_mask
riverctl map normal Super       Tab focus-previous-tags



##############
# Media Keys #
##############
for mode in normal locked
do
    case "$(hostname)" in
        "m5")
            # This laptop does not automatically turn the screen off when the
            # lid is closed (maybe the switch  broke or libreboot simply does
            # not support it), so I need to do it manually.
            riverctl map $mode None XF86Launch1 spawn "wlopm --toggle LVDS-1"

            # The BIOS tries to be helpful and handles the brightness keys
            # itself on this laptop while for whatever reason still sending the
            # keycodes. So I can't bind them here otherwise BIOS ans OS will
            # get in each others way trying to set the brightness. Sucks,
            # because when the BIOS sets the brightness it randomly resets in
            # response to certain events, such as toggling the monitor.
            # TODO: re-flash libreboot and try to turn this off
            ;;

        *)
            riverctl map $mode None XF86MonBrightnessUp   spawn "light -A 5"
            riverctl map $mode None XF86MonBrightnessDown spawn "light -U 5"
            ;;
    esac

    riverctl map $mode None XF86Eject             spawn "eject -T"
    riverctl map $mode None XF86AudioRaiseVolume  spawn "pamixer -i 5"
    riverctl map $mode None XF86AudioLowerVolume  spawn "pamixer -d 5"
    riverctl map $mode None XF86AudioMute         spawn "pamixer --set-volume 0"
    riverctl map $mode None XF86AudioMedia        spawn "playerctl play-pause"
    riverctl map $mode None XF86AudioPlay         spawn "playerctl play-pause"
    riverctl map $mode None XF86AudioPrev         spawn "playerctl previous"
    riverctl map $mode None XF86AudioNext         spawn "playerctl next"
done


###########
# Pointer #
###########
riverctl map-pointer normal Super BTN_LEFT move-view
riverctl map-pointer normal Super BTN_RIGHT resize-view
riverctl focus-follow-cursor normal
riverctl set-cursor-warp on-output-change


##########
# Filter #
##########
riverctl float-filter-add app-id launcher
riverctl float-filter-add app-id popup
riverctl float-filter-add app-id lhp.Snayk
riverctl float-filter-add app-id com.rafaelmardojai.Blanket
riverctl csd-filter-add app-id gnome-mines
riverctl csd-filter-add app-id gnome-chess
riverctl csd-filter-add app-id gnome-system-monitor
riverctl csd-filter-add app-id org.gnome.Nautilus
#riverctl csd-filter-add app-id iceweasel # Thanks to userChrome.css this is no longer necessary
riverctl csd-filter-add app-id com.rafaelmardojai.Blanket
riverctl csd-filter-add app-id io.github.lainsce.DotMatrix
