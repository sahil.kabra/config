#!/usr/bin/env bash

##############
# Appearance #
##############
riverctl background-color 0x002b36
riverctl border-color-focused 0x990000
riverctl border-color-unfocused 0x586e75
riverctl border-width 1
#riverctl xcursor-theme tarazed

##########
# Inputs #
##########
riverctl set-repeat 40 300
riverctl input pointer-1149-8264-Primax_Kensington_Eagle_Trackball left-handed enabled
# riverctl input 1149:8264:Primax_Kensington_Eagle_Trackball left-handed enabled
# riverctl input 1267:12572:DELL097D:00_04F3:311C_Touchpad tap enabled
riverctl input pointer-1267-12572-DELL097D:00_04F3:311C_Touchpad tap enabled

###########
# Interop #
###########
riverctl spawn "systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=river"
riverctl spawn "dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=river"


#########
# Modes #
#########
modes=("locked" "command" "terminal" "app" "browser" "system" "windows" "tag" "tag_set" "layout")

for mode in ${modes[@]}
do
  riverctl declare-mode $mode
  riverctl map $mode None Escape enter-mode normal
  riverctl map $mode Super c enter-mode command
done

############
# Keybinds #
############
riverctl map normal Super+Shift Return spawn "${HOME}/.bin/term prs"

# command mode mappings
riverctl map normal Super c enter-mode command

# command mode to other modes
riverctl map command None  a enter-mode app
riverctl map command None  s enter-mode system
riverctl map command None  t enter-mode terminal
riverctl map command None  w enter-mode windows
riverctl map command None  l enter-mode layout
riverctl map command None  d enter-mode tag

#        launchers
riverctl map command None  p spawn "wofi --show=run"
riverctl map command Shift Q exit
riverctl map command None  q close


#        terminals
riverctl map terminal None p spawn "${HOME}/.bin/term prs"
riverctl map terminal None w spawn "${HOME}/.bin/term work"

#        app launchers
riverctl map app None b enter-mode browser

riverctl map app None i spawn "${HOME}/.bin/idea"
riverctl map app None m spawn "${HOME}/.bin/apps slackw"
riverctl map app None s spawn "${HOME}/.bin/screenshot"
riverctl map app None r spawn "${HOME}/.bin/record"

#        browsers
riverctl map browser None b spawn "brave --enable-features=UseOzonePlatform --ozone-platform=wayland"
riverctl map browser None c spawn "${HOME}/.bin/chrome"
riverctl map browser None f spawn "firefox"
riverctl map browser None q spawn "qutebrowser"
riverctl map browser None v spawn "vimb"


#        system
riverctl map system Shift H spawn "poweroff"
riverctl map system Shift R spawn "reboot"
riverctl map system Shift l spawn "waylock --init-color '#cccccc' --input-color '#115511'"

# layout
riverctl map layout None t send-layout-cmd rivertile "main-ratio 0.7"
riverctl map layout None d send-layout-cmd rivertile "main-ratio 0.5"
riverctl map layout None h send-layout-cmd rivertile "main-location top"
riverctl map layout None v send-layout-cmd rivertile "main-location left"
riverctl map layout None p send-layout-cmd rivertile "main-count +1"
riverctl map layout None m send-layout-cmd rivertile "main-count -1"


#        windows
riverctl map windows None j focus-view next
riverctl map windows None k focus-view previous

#riverctl map windows None w focus-output DP-3
#riverctl map windows None e focus-output eDP-1
riverctl map windows None w focus-output next
riverctl map windows None e focus-output next

#riverctl map windows Shift W send-to-output DP-3
#riverctl map windows Shift E send-to-output eDP-1
riverctl map windows Shift W send-to-output next
riverctl map windows Shift E send-to-output next

riverctl map windows None  z zoom
riverctl map windows Shift Z toggle-fullscreen
riverctl map windows Shift F toggle-float
riverctl map windows Shift H send-layout-cmd rivertile "main-ratio -0.05"
riverctl map windows Shift L send-layout-cmd rivertile "main-ratio +0.05"
riverctl map windows None  t enter-mode      tag
riverctl map tag     None  s enter-mode      tag_set

for i in $(seq 1 9)
do
    tags=$((1 << ($i - 1)))

    riverctl map tag_set None $i set-view-tags $tags
    # riverctl map tag_set None $i toggle-view-tags $tags

    riverctl map tag None $i toggle-focused-tags $tags
    # riverctl map tag_view None $i toggle-focused-tags $tags
done

all_tags=$(((1 << 32) - 1))
riverctl map tag Shift A set-focused-tags $all_tags


riverctl map-pointer normal Super BTN_LEFT move-view
riverctl map-pointer normal Super BTN_RIGHT resize-view


riverctl map normal None XF86AudioRaiseVolume  spawn '${HOME}/.bin/volume     up'
riverctl map normal None XF86AudioLowerVolume  spawn '${HOME}/.bin/volume     down'
riverctl map normal None XF86AudioMute         spawn '${HOME}/.bin/volume     mute'
riverctl map $mode  None XF86MonBrightnessUp   spawn '${HOME}/.bin/brightness up'
riverctl map $mode  None XF86MonBrightnessDown spawn '${HOME}/.bin/brightness down'

################
# Applications #
################
riverctl spawn waybar
riverctl spawn "gammastep"
riverctl spawn "wlr-randr --output eDP-1 --custom-mode 2560x1600@60Hz"
riverctl spawn kanshi

##################
# Layout manager #
##################
riverctl default-layout rivertile
riverctl spawn 'rivertile -view-padding 1 -outer-padding 0 -main-ratio 0.5'

##########
# Filter #
##########
riverctl float-filter-add app-id launcher
riverctl float-filter-add app-id popup
