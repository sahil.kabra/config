import Data.Map.Strict (Map)
import System.Exit (exitSuccess)
import XMonad
import XMonad.Actions.CycleWS (nextWS, prevWS, toggleWS)
import XMonad.Actions.EasyMotion (selectWindow)
import XMonad.Actions.WindowBringer
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Layout.BoringWindows (boringWindows)
import XMonad.Layout.IndependentScreens
import XMonad.Layout.Magnifier as M
import XMonad.Layout.MultiToggle as Mt
import XMonad.Layout.MultiToggle.Instances as Mti
import XMonad.Layout.NoBorders (smartBorders)
import XMonad.Layout.Renamed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.SubLayouts (subTabbed)
import XMonad.Layout.Tabbed (fontName, shrinkText, tabbed)
import XMonad.Layout.TwoPane
import XMonad.Layout.WindowNavigation (windowNavigation)
import qualified XMonad.StackSet as W
import XMonad.Util.Cursor (setDefaultCursor)
import XMonad.Util.EZConfig
import XMonad.Util.Hacks (javaHack)
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Ungrab

myBar = "/home/sahil-kabra_skyfii/.local/bin/xmobar"
myMenu = "rofi"

main :: IO ()
main =
  xmonad
    . ewmhFullscreen
    . ewmh
    . withEasySB (statusBarProp myBar (pure def)) defToggleStrutsKey
    . javaHack
    $ myConfig

myConfig =
  def
    { keys = myKeys
      , modMask = myModMask
      , layoutHook = myLayout
      , terminal = prsTerminal
      , workspaces = myWorkspaces
      , normalBorderColor = myNormalBorderColor
      , focusedBorderColor = myFocusedBorderColor
      , borderWidth = myBorderWidth
      , startupHook = setDefaultCursor xC_left_ptr
    }

myLayout =
  smartBorders
    . Mt.mkToggle (Mti.FULL ?? MIRROR ?? Mt.EOT)
    $ m ||| halfTiled ||| quarterTiled ||| tb ||| tp ||| myColMid
  where
    tiled = Tall nmaster delta ratio
    nmaster = 1
    delta = 3 / 100
    ratio = 2 / 3
    halfTile = Tall nmaster delta (1/2)
    myThreeColMid = ThreeColMid nmaster delta ratio
    m = renamed [Replace "tall"] $ magnifiercz' 1 tiled
    tb = renamed [Replace "tabbed"] $ tabbed shrinkText def {fontName = "xft:Hack:size=10"}
    tp = renamed [Replace "two pane"] $ TwoPane delta (1 / 2)
    quarterTiled = renamed [Replace "quarter tiled"] $windowNavigation $ subTabbed $ boringWindows tiled
    halfTiled =  renamed [Replace "half tiled"] $ windowNavigation $ subTabbed $ boringWindows halfTile
    myColMid = renamed [Replace "3 col mid"] $ myThreeColMid

-- Key aliases - useful to refer to keys by name
lAltKey = mod1Mask

numLockKey = mod2Mask

winKey = mod4Mask

rAltKey = mod5Mask

myModMask = winKey

modKey = "M"

alongWith = "-"

space = " "

workTerminal = "${HOME}/.bin/term work"

prsTerminal = "${HOME}/.bin/term prs"

myNormalBorderColor = "#DDDDDD"
myFocusedBorderColor = "#2222FF"
myBorderWidth = 1

myWorkspaces =
  withScreens
    3
    (
      ["hom:0", "trm:1", "ide:2", "prs:3", "ffx:4", "hq:5", "msc:6", "msc:7", "scr:8", "msg:9"]
    )

myKeys :: XConfig l -> Map (KeyMask, KeySym) (X ())
myKeys c = mkKeymap c $ myKeyMap c

myKeyMap :: XConfig l -> [(String, X ())]
myKeyMap c =
  commandSubmap c
    ++ [ (modKey ++ alongWith ++ "<Space>", sendMessage NextLayout),
         ("<XF86MonBrightnessUp>", spawn "$HOME/.bin/brightness up"),
         ("<XF86MonBrightnessDown>", spawn "$HOME/.bin/brightness down"),
         ("<XF86AudioRaiseVolume>", spawn "$HOME/.bin/volume up"),
         ("<XF86AudioLowerVolume>", spawn "$HOME/.bin/volume down"),
         ("<XF86AudioMute>", spawn "$HOME/.bin/volume mute")
       ]

-- M-c
commandMode = modKey ++ alongWith ++ "c "

commandSubmap :: XConfig l -> [(String, X ())]
commandSubmap c =
  [ (modeKey ++ key, action)
    | modeKey <- [commandMode],
      (key, action) <-
        [ ("p", spawn (myMenu ++ " -show run")),
          ("q", kill),
          ("S-q", io exitSuccess),
          ("S-r", spawn "xmonad --recompile && xmonad --restart")
        ]
          ++ termSubmap
          ++ appSubmap
          ++ systemSubmap
          ++ desktopSubmap c
          ++ windowSubmap c
  ]

-- M-c t
termMode = "t "

termSubmap :: [(String, X ())]
termSubmap =
  [ (modeKey ++ key, action)
    | modeKey <- [termMode],
      (key, action) <-
        [ ("w", spawn workTerminal),
          ("p", spawn prsTerminal)
        ]
  ]

-- M-c a
appMode = "a "

appSubmap :: [(String, X ())]
appSubmap =
  [ (modeKey ++ key, action)
    | modeKey <- [appMode],
      (key, action) <-
        [ ("i", spawn "${HOME}/.bin/idea"),
          ("m", spawn "${HOME}/.bin/apps slack"),
          ("s", unGrab *> spawn "${HOME}/.bin/screenshot")
        ]
          ++ browserSubmap
  ]

-- M-c a b
browserMode = "b "

browserSubmap :: [(String, X ())]
browserSubmap =
  [ (modeKey ++ key, action)
    | modeKey <- [browserMode],
      (key, action) <-
        [ ("b", spawn "brave"),
          -- ("c", spawn "google-chrome-beta"),
          ("d", spawn "${HOME}/.bin/browser"),
          ("f", spawn "${HOME}/.bin/firefox"),
          ("q", spawn "${HOME}/.bin/qutebrowser")
        ]
        ++ chromeSubmap
  ]

-- M-c a b c
chromeMode = "c "

chromeSubmap :: [(String, X ())]
chromeSubmap =
  [ (modeKey ++ key, action)
    | modeKey <- [chromeMode],
      (key, action) <-
          [ ("w", spawn "google-chrome-beta --profile-directory='Profile 1'")
          , ("p", spawn "google-chrome-beta --profile-directory='Profile 4'")
        ]
  ]

-- M-c s
sysMode = "s "

systemSubmap :: [(String, X ())]
systemSubmap =
  [ (modeKey ++ key, action)
    | modeKey <- [sysMode],
      (key, action) <-
        [ ("S-h", spawn "/usr/sbin/poweroff"),
          ("S-r", spawn "/usr/sbin/reboot"),
          ("S-s", spawn "${HOME}/.bin/suspend-laptop"),
          ("S-l", spawn "slock")
        ]
  ]

-- M-c d
desktopMode = "d "
desktopSubmap :: XConfig l -> [(String, X ())]
desktopSubmap c =
  [ (modeKey ++ key, action)
    | modeKey <- [desktopMode],
      (key, action) <-
          [
            -- go to next ws
            ("n", nextWS),
            ("p", prevWS)
          ]
          ++
            -- move to workspace
          [ (m ++ k, windows $ onCurrentScreen f i)
              | (i, k) <- zip (workspaces' c) $ [show n | n <- [0 .. 9]],
                (f, m) <- [(W.greedyView, "d "), (W.shift, "m ")]
          ]
  ]

-- M-c w
windowMode = "w "

windowBringerConfig =
  WindowBringerConfig
    { menuCommand = "dmenu",
      menuArgs = ["-i"],
      windowTitler = decorateName,
      windowFilter = \_ -> return True
    }

-- copied from window bringer
decorateName :: XMonad.WindowSpace -> Window -> X String
decorateName ws w = do
  name <- show <$> getName w
  return $ name ++ " [" ++ W.tag ws ++ "]"

windowSubmap :: XConfig l -> [(String, X ())]
windowSubmap c =
  [ (modeKey ++ key, action)
    | modeKey <- [windowMode],
      (key, action) <-
        [ ("q", kill), -- kill window
        -- move to screen
          ("e", screenWorkspace 0 >>= flip whenJust (windows . W.view)),
          ("w", screenWorkspace 1 >>= flip whenJust (windows . W.view)),
          ("r", screenWorkspace 2 >>= flip whenJust (windows . W.view)),
          -- move client to screen
          ("S-e", screenWorkspace 0 >>= flip whenJust (windows . W.shift)),
          ("S-w", screenWorkspace 1 >>= flip whenJust (windows . W.shift)),
          ("S-r", screenWorkspace 2 >>= flip whenJust (windows . W.shift)),
          -- bring window
          ("b", bringMenuConfig windowBringerConfig),
          -- go to window
          ("g", gotoMenuConfig windowBringerConfig),
          -- go to recent ws
          ("T", toggleWS),
          -- tile floating window
          ("t", withFocused $ windows . W.sink),
          -- move between windows
          ("j", windows W.focusUp),
          ("k", windows W.focusDown),
          -- swap master with focussed
          ("s m", windows W.swapMaster),
          ("f m", windows W.focusMaster),
          -- zoom current window
          ("z", sendMessage $ Mt.Toggle Mti.FULL),
          ("m", sendMessage $ Mt.Toggle Mti.MIRROR)
        ]
  ]
